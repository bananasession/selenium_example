package utils;
import org.openqa.selenium.By;

import static org.testng.Assert.*;

public class VerifyAccount {

    private static final By LOCATE_ERR_FIRST_NAME = By.xpath("//input[@id=\"input-firstname\"]//..//div[@class=\"text-danger\"]");
    private static final By LOCATE_ERR_LAST_NAME = By.xpath("//input[@id=\"input-lastname\"]//..//div[@class=\"text-danger\"]");
    private static final By LOCATE_ERR_EMAIL = By.xpath("//input[@id=\"input-email\"]//..//div[@class=\"text-danger\"]");
    private static final By LOCATE_ERR_TELEPHONE = By.xpath("//input[@id=\"input-telephone\"]//..//div[@class=\"text-danger\"]");
    private static final By LOCATE_ERR_PASSWORD = By.xpath("//input[@id=\"input-password\"]//..//div[@class=\"text-danger\"]");
    private static final By LOCATE_WARNING = By.cssSelector(".alert.alert-danger.alert-dismissible");

    public static void verify() {
        assertEquals(Browser.driver.getTitle(), "Your Account Has Been Created!", "Wrong page!");
    }

    public static void verifyLoginFields(String warning, String firstName, String lastName, String email, String telephone, String password){
        assertTrue(Browser.driver.findElement(LOCATE_WARNING).getText().contains(warning));
        assertTrue(Browser.driver.findElement(LOCATE_ERR_FIRST_NAME).getText().contains(firstName));
        assertTrue(Browser.driver.findElement(LOCATE_ERR_LAST_NAME).getText().contains(lastName));
        assertTrue(Browser.driver.findElement(LOCATE_ERR_EMAIL).getText().contains(email));
        assertTrue(Browser.driver.findElement(LOCATE_ERR_TELEPHONE).getText().contains(telephone));
        assertTrue(Browser.driver.findElement(LOCATE_ERR_PASSWORD).getText().contains(password));
    }

    public static void verifyLogin(){
        assertEquals(Browser.driver.getTitle(), "My Account", "Something happened af");
    }
}
