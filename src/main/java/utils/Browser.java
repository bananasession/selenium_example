package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

public class Browser {

    private static final By LOCATE_ACCOUNT_FORM = By.cssSelector(".list-inline li:nth-child(2)");
//    .list-inline li:nth-child(2)


    public static WebDriver driver;

    public static void open() {
        String driverPath = Paths.get("chromedriver").toAbsolutePath().toString();
        System.setProperty("webdriver.chrome.driver", driverPath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public static void goTo() {
        Browser.driver.get("http://shop.pragmatic.bg/");
    }

    public static void myAccount(){
        Browser.driver.findElement(LOCATE_ACCOUNT_FORM).click();
    }

    public static void close() {
        driver.quit();
    }
}
