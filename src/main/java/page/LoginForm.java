package page;

import org.openqa.selenium.By;
import utils.Browser;
import utils.UserData;

public class LoginForm {
    private static final By LOCATE_LOGIN_FORM = By.cssSelector(".dropdown-menu-right li:nth-child(2)");
    private static final By LOCATE_INPUT_EMAIL = By.id("input-email");
    private static final By LOCATE_INPUT_PASSWORD = By.id("input-password");
    private static final By LOCATE_LOGIN_BTN = By.xpath("//input[@value=\"Login\"]");

    public static void navigateToLogin(){
        Browser.driver.findElement(LOCATE_LOGIN_FORM).click();
    }

    public static void fillLogin(){
        Browser.driver.findElement(LOCATE_INPUT_EMAIL).sendKeys(UserData.username);
        Browser.driver.findElement(LOCATE_INPUT_PASSWORD).sendKeys(UserData.password);
        Browser.driver.findElement(LOCATE_LOGIN_BTN).click();

    }
}
