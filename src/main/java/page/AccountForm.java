package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import sun.jvm.hotspot.utilities.Assert;
import utils.Browser;
import static org.testng.Assert.*;

public class AccountForm {

    private static final By LOCATE_REG_FORM = By.xpath("//ul[@class=\"dropdown-menu dropdown-menu-right\"]/li/a");
    private static final By LOCATE_FIRST_NAME = By.id("input-firstname");
    private static final By LOCATE_LAST_NAME = By.id("input-lastname");
    private static final By LOCATE_EMAIL = By.id("input-email");
    private static final By LOCATE_TELEPHONE = By.id("input-telephone");
    private static final By LOCATE_PASSWORD = By.id("input-password");
    private static final By LOCATE_CONFIRM_PASSWORD = By.id("input-confirm");
    private static final By LOCATE_PRIVACY_POLICY = By.xpath("//input[@type='checkbox']");
    private static final By LOCATE_SUBSCRIBE = By.name("newsletter");
    private static final By LOCATE_LOGIN_BTN = By.xpath("//input[@value=\"Continue\"]");

    public static void navigateToRegister() {
        Browser.driver.findElement(LOCATE_REG_FORM).click();
    }

    public static void fillRegister(String firstName, String lastName, String email, String telephone, String password, String confirmPsswrd) {
        Browser.driver.findElement(LOCATE_FIRST_NAME).sendKeys(firstName);
        Browser.driver.findElement(LOCATE_LAST_NAME).sendKeys(lastName);
        Browser.driver.findElement(LOCATE_EMAIL).sendKeys(email);
        Browser.driver.findElement(LOCATE_TELEPHONE).sendKeys(telephone);
        Browser.driver.findElement(LOCATE_PASSWORD).sendKeys(password);
        Browser.driver.findElement(LOCATE_CONFIRM_PASSWORD).sendKeys(confirmPsswrd);
    }


    public static void checkBox(){
        WebElement checkBox = Browser.driver.findElement(LOCATE_PRIVACY_POLICY);
        if (!checkBox.isSelected()){
            checkBox.click();
        }

        assertTrue(checkBox.isSelected());
    }


    public static void radioBtn() {
        WebElement yesBtn = Browser.driver.findElement(LOCATE_SUBSCRIBE);
        if (!yesBtn.isSelected()){
            yesBtn.click();
        }

        assertTrue(yesBtn.isSelected());
    }

    public static void clickLogin() {
        Browser.driver.findElement(LOCATE_LOGIN_BTN).click();
    }
}