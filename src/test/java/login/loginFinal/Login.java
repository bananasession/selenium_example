package login.loginFinal;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page.LoginForm;
import utils.Browser;
import utils.VerifyAccount;

public class Login {

    @BeforeMethod
    public void setUp() {
        Browser.open();
    }

    @Test
    public void testName() {
        Browser.goTo();
        Browser.myAccount();
        LoginForm.navigateToLogin();
        LoginForm.fillLogin();
        VerifyAccount.verifyLogin();
    }



    @AfterMethod
    public void tearDown() {
        Browser.close();
    }
}
