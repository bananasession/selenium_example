package login.negative;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page.AccountForm;
import sun.plugin.services.BrowserService;
import utils.Browser;
import utils.VerifyAccount;

public class CreateAccountTest {

    private static final String ERR_WARNING = "Warning: You must agree to the Privacy Policy!";
    private static final String ERR_FIRST_NAME = "First Name must be between 1 and 32 characters!";
    private static final String ERR_LAST_NAME = "Last Name must be between 1 and 32 characters!";
    private static final String ERR_EMAIL = "E-Mail Address does not appear to be valid!";
    private static final String ERR_TELEPHONE = "Telephone must be between 3 and 32 characters!";
    private static final String ERR_CONFIRM_PASSWORD = "Password must be between 4 and 20 characters!";

    @BeforeMethod
    public void setUp() {
        Browser.open();
    }

    @Test
    public void testName() {
        Browser.goTo();
        Browser.myAccount();
        AccountForm.navigateToRegister();
        AccountForm.clickLogin();
        VerifyAccount.verifyLoginFields(ERR_WARNING, ERR_FIRST_NAME, ERR_LAST_NAME, ERR_EMAIL,ERR_TELEPHONE , ERR_CONFIRM_PASSWORD);
    }

    @AfterMethod
    public void tearDown() {
        Browser.close();
    }
}
