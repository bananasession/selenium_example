package login.positive;

import net.bytebuddy.utility.RandomString;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page.AccountForm;
import utils.Browser;
import utils.UserData;
import utils.VerifyAccount;

import java.util.Random;

public class CreateAccountTest {

    @BeforeMethod
    public void setUp() {
        Browser.open();
    }

    @Test
    public void testName() {
        Browser.goTo();
        Browser.myAccount();
        AccountForm.navigateToRegister();
        AccountForm.fillRegister("Georgi", "Krumov", UserData.username, "62265", UserData.password, "parola123");
        AccountForm.checkBox();
        AccountForm.radioBtn();
        AccountForm.clickLogin();
        VerifyAccount.verify();
    }

    @AfterMethod
    public void tearDown() {
        Browser.close();
    }
}
